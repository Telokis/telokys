module.exports = {
    env: {
        jest: true,
    },
    extends: ["plugin:jest/recommended"],
    plugins: ["jest"],
    rules: {
        "jest/consistent-test-it": "error",
        "jest/prefer-strict-equal": "error",
        "jest/valid-describe-callback": "error",
    },
    overrides: [
        {
            files: ["**/tests/**/*.test.js", "**/__tests__/**/*.test.js"],
            extends: ["plugin:jest/recommended"],
        },
    ],
};

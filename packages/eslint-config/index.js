module.exports = {
    env: {
        node: true,
    },
    extends: ["airbnb-base", "prettier"],
    parserOptions: {
        ecmaVersion: 8,
        sourceType: "module",
    },
    ignorePatterns: ["node_modules"],
    settings: {
        "import/extensions": [".js"],
    },
    plugins: ["prettier"],
    rules: {
        "import/extensions": [
            "error",
            "always",
            {
                js: "never",
            },
        ],
        "object-shorthand": ["error", "always"],
        "prettier/prettier": "error",
        "class-methods-use-this": "off",
        "consistent-return": "off",
        "no-plusplus": "off",
        "no-return-await": "off",
        "import/no-dynamic-require": "off",
        "import/prefer-default-export": "off",
        "no-loop-func": "off",
        "no-underscore-dangle": "off",
        "no-console": "warn",
        "no-param-reassign": "off",
        "global-require": "off",
        "no-await-in-loop": "off",
        camelcase: "off",
        "no-bitwise": "off",
        "no-restricted-globals": "off",
        "no-nested-ternary": "off",
        "import/no-extraneous-dependencies": "off",
        "prefer-destructuring": [
            "error",
            {
                VariableDeclarator: {
                    object: true,
                },
            },
        ],
        "no-restricted-syntax": "off",
    },
    overrides: [
        {
            files: ["**/tests/**/*.test.js", "**/__tests__/**/*.test.js"],
            extends: [require.resolve("./jest")],
        },
    ],
};

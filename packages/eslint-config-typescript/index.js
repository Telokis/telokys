module.exports = {
    env: {
        node: true,
    },
    extends: ["plugin:@typescript-eslint/recommended", "@telokys/eslint-config"],
    parserOptions: {
        ecmaVersion: 8,
        sourceType: "module",
    },
    ignorePatterns: ["node_modules"],
    settings: {
        "import/resolver": {
            node: {
                extensions: [".js", ".ts"],
            },
        },
        "import/extensions": [".js", ".ts"],
    },
    plugins: ["@typescript-eslint"],
    parser: "@typescript-eslint/parser",
    rules: {
        "import/extensions": [
            "error",
            "always",
            {
                js: "never",
                ts: "never",
            },
        ],
        "no-undef": "off",
        "no-dupe-class-members": "off",
        "no-shadow": "off",
        "no-use-before-define": "off",
        "@typescript-eslint/no-use-before-define": "error",
        "@typescript-eslint/no-shadow": "error",
        "babel/no-unused-expressions": "off",
        "@typescript-eslint/explicit-function-return-type": "off",
        "@typescript-eslint/no-non-null-assertion": "off",
        "@typescript-eslint/no-var-requires": "off",
        "@typescript-eslint/ban-ts-ignore": "off",
        "@typescript-eslint/camelcase": "off",
        "@typescript-eslint/interface-name-prefix": "off",
        "@typescript-eslint/no-empty-interface": "off",
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "@typescript-eslint/ban-ts-comment": "off",
        "@typescript-eslint/no-unused-expressions": "error",
        "@typescript-eslint/no-unused-vars": "error",
        "no-unused-expressions": "off",
        "no-unused-vars": "off",
    },
    overrides: [
        {
            files: [
                "**/tests/**/*.test.js",
                "**/__tests__/**/*.test.js",
                "**/tests/**/*.test.ts",
                "**/__tests__/**/*.test.ts",
            ],
            extends: [require.resolve("./jest")],
        },
    ],
};

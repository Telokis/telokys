/**
 * Extracts the type of elements from an array type.
 *
 * @template ArrayType - The array type to extract elements from. Must extend `readonly unknown[]`
 * @returns The type of elements contained in the array. If `ArrayType` is not an array, returns `never`
 *
 * @example
 * ```typescript
 * type StringArray = string[];
 * type Element = ArrayElement<StringArray>; // string
 *
 * type ReadonlyNumberArray = readonly number[];
 * type NumberElement = ArrayElement<ReadonlyNumberArray>; // number
 * ```
 */
type ArrayElement<
    ArrayType extends readonly unknown[]
> = ArrayType extends readonly (infer ElementType)[] ? ElementType : never;

export { ArrayElement };

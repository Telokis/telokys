import { NonNull } from "./NonNull";

/**
 * Makes specified properties of a type required, keeping others unchanged.
 *
 * @template Terface - The interface/type to modify
 * @template K - Keys to make required (defaults to all keys)
 * @template RemainingKeys - Remaining keys to leave unchanged
 *
 * @example
 * ```typescript
 * interface User {
 *  name?: string;
 *  age?: number;
 *  id: string;
 *  profile?: {
 *   name?: string
 *  };
 * }
 * type User_Required = MakeRequired<User, 'name' | 'profile'>;
 *
 * // Equivalent to
 *
 * type User_Required = {
 *  name: string;
 *  age?: number;
 *  id: string;
 *  profile: {
 *   name?: string
 *  };
 * }
 * ```
 */
type MakeRequired<
    Terface,
    K extends keyof Terface = keyof Terface,
    RemainingKeys extends keyof Terface = Exclude<keyof Terface, K>,
> = Required<{ [P in K]: Terface[P] }> & { [P in RemainingKeys]: Terface[P] };

export { MakeRequired };

/**
 * Makes all properties in a type required recursively, including nested objects.
 *
 * @template Terface - The interface/type to make deeply required
 *
 * @example
 * ```typescript
 * interface Nested { data?: { value?: number } }
 * type Nested_Required = DeepRequired<Nested>; // { data: { value: number } }
 * ```
 */
export type DeepRequired<Terface> = Required<{ [P in keyof Terface]: DeepRequired<Terface[P]> }>;

/**
 * Makes specified properties non-null, keeping others unchanged.
 *
 * @template Terface - The interface/type to modify
 * @template K - Keys to make non-null (defaults to all keys)
 * @template RemainingKeys - Remaining keys to leave unchanged
 *
 * @example
 * ```typescript
 * interface Data { value: string | null; id: number | null; }
 * type NonNull = MakeNonNull<Data, 'value'>; // { value: string; id: number | null; }
 * ```
 */
type MakeNonNull<
    Terface,
    K extends keyof Terface = keyof Terface,
    RemainingKeys extends keyof Terface = Exclude<keyof Terface, K>,
> = { [P in K]: NonNull<Terface[P]> } & {
    [P in RemainingKeys]: Terface[P];
};

export { MakeNonNull };

import { PickMembers } from "./PickMembers";
import { KeysWhereTypeCannotBe } from "./KeysWhereTypeCannotBe";

type PickMembersWhereTypeCannotBe<Terface, Filter> = PickMembers<
    Terface,
    KeysWhereTypeCannotBe<Terface, Filter>
>;

export { PickMembersWhereTypeCannotBe };

export { PickMembersWhereTypeCannotBe as PickMembersWhereTypeCantBe };

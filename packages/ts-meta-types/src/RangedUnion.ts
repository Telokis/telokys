import { ArrayElement } from "./ArrayElement";

type ComputeRange<N extends number, Result extends Array<unknown> = []> = Result["length"] extends N
    ? Result
    : ComputeRange<N, [...Result, Result["length"]]>;

type Add<A extends number, B extends number> = [...ComputeRange<A>, ...ComputeRange<B>]["length"];

type IsGreater<A extends number, B extends number> = IsLiteralNumber<
    [...ComputeRange<B>][Last<[...ComputeRange<A>]>]
> extends true
    ? false
    : true;

type Last<T extends any[]> = T extends [...infer _, infer Last]
    ? Last extends number
        ? Last
        : never
    : never;

type RemoveLast<T extends any[]> = T extends [...infer Rest, infer _] ? Rest : never;

type IsLiteralNumber<N> = N extends number ? (number extends N ? false : true) : false;

type AddIteration<
    Min extends number,
    Max extends number,
    Step extends number = 1,
    Result extends Array<unknown> = [Min],
> = IsGreater<Last<Result>, Max> extends true
    ? RemoveLast<Result>
    : AddIteration<Min, Max, Step, [...Result, Add<Last<Result>, Step>]>;

/**
 * Creates a union type of numbers within a specified range with an optional step increment.
 *
 * @template Min - The minimum value of the range (inclusive)
 * @template Max - The maximum value of the range (inclusive)
 * @template [Step=1] - The increment value between each number in the range
 *
 * @example
 * ```typescript
 * type Numbers = RangedUnion<0, 5>; // 0 | 1 | 2 | 3 | 4 | 5
 * type EvenNumbers = RangedUnion<0, 6, 2>; // 0 | 2 | 4 | 6
 * type AboveTen = RangedUnion<10, 20, 3>; // 10 | 13 | 16 | 19
 * ```
 */
export type RangedUnion<
    Min extends number,
    Max extends number,
    Step extends number = 1,
> = ArrayElement<AddIteration<Min, Max, Step>>;

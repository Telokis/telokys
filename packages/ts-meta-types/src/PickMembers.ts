type PickMembers<Terface, K extends keyof Terface> = { [P in K]: Terface[P] };

export { PickMembers };

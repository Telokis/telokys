import { PickMembers } from "./PickMembers";
import { KeysWhereTypeCanBe } from "./KeysWhereTypeCanBe";

type PickMembersWhereTypeCanBe<Terface, Filter> = PickMembers<
    Terface,
    KeysWhereTypeCanBe<Terface, Filter>
>;

export { PickMembersWhereTypeCanBe };

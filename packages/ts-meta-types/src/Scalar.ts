export type Scalar = string | boolean | number;

export type NullableScalar = Scalar | null;

export type OptionalScalar = Scalar | undefined;

export type OptionalNullableScalar = NullableScalar | undefined;

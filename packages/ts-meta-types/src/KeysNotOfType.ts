import { KeysOfType } from "./KeysOfType";

type KeysNotOfType<Terface, Filter> = Exclude<keyof Terface, KeysOfType<Terface, Filter>>;

export { KeysNotOfType };

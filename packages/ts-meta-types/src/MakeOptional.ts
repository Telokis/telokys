/**
 * Makes specified properties of a type required, keeping others unchanged.
 *
 * @template Terface - The interface/type to modify
 * @template K - Keys to make required (defaults to all keys)
 * @template RemainingKeys - Remaining keys to leave unchanged
 *
 * @example
 * ```typescript
 * interface User {
 *  name?: string;
 *  age?: number;
 *  id: string;
 *  profile?: {
 *   name?: string
 *  };
 * }
 * type User_Optional = MakeOptional<User, 'name' | 'profile'>;
 *
 * // Equivalent to
 *
 * type User_Optional = {
 *  name: string;
 *  age?: number;
 *  id: string;
 *  profile: {
 *   name?: string
 *  };
 * }
 * ```
 */
type MakeOptional<
    Terface,
    K extends keyof Terface = keyof Terface,
    RemainingKeys extends keyof Terface = Exclude<keyof Terface, K>,
> = Partial<{ [P in K]: Terface[P] }> & { [P in RemainingKeys]: Terface[P] };

export { MakeOptional };

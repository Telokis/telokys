interface ObjectOf<T> {
    [key: string]: T;
}

export { ObjectOf };

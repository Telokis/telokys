import { KeysWhereTypeCanBe } from "./KeysWhereTypeCanBe";

type KeysWhereTypeCannotBe<Terface, Filter> = Exclude<
    keyof Terface,
    KeysWhereTypeCanBe<Terface, Filter>
>;

export { KeysWhereTypeCannotBe };

export { KeysWhereTypeCannotBe as KeysWhereTypeCantBe };

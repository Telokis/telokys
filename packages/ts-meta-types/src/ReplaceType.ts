import { KeysOfType } from "./KeysOfType";
import { KeysWhereTypeCanBe } from "./KeysWhereTypeCanBe";
import { PickMembersNotOfType } from "./PickMembersNotOfType";
import { PickMembersWhereTypeCannotBe } from "./PickMembersWhereTypeCannotBe";

type ReplaceType<Terface, Filter, New> = PickMembersNotOfType<Terface, Filter> &
    {
        [P in KeysOfType<Terface, Filter>]: New;
    };

export { ReplaceType };

type ReplaceTypeIfCanBe<Terface, Filter, New> = PickMembersWhereTypeCannotBe<Terface, Filter> &
    {
        [P in KeysWhereTypeCanBe<Terface, Filter>]: New;
    };

export { ReplaceTypeIfCanBe };

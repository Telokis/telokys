import { PickMembers } from "./PickMembers";
import { KeysOfType } from "./KeysOfType";

type PickMembersOfType<Terface, Filter> = PickMembers<Terface, KeysOfType<Terface, Filter>>;

export { PickMembersOfType };

type PickVoidMembers<Terface> = PickMembersOfType<Terface, void>;

export { PickVoidMembers };

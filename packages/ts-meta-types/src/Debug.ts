// Helpers taken from https://stackoverflow.com/questions/57683303/how-can-i-see-the-full-expanded-contract-of-a-typescript-type

type Debug<Terface> = Terface extends infer O ? { [K in keyof O]: O[K] } : never;

export { Debug };

// eslint-disable-next-line @typescript-eslint/ban-types
type DebugR<Terface> = Terface extends object
    ? Terface extends infer O
        ? { [K in keyof O]: DebugR<O[K]> }
        : never
    : Terface;

export { DebugR };

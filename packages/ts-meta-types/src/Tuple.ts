/**
 * Generate a type that is a fixed length array of the given type.
 *
 * @example
 * ```ts
 * type Tuple3 = Tuple<string, 3>;
 * //	^ = type Tuple3 = [string, string, string]
 * ```
 */
type Tuple<Type, N extends number, T extends Type[] = []> = N extends T["length"]
    ? T
    : Tuple<Type, N, [...T, Type]>;

export { Tuple };

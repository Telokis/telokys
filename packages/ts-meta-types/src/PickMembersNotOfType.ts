import { PickMembers } from "./PickMembers";
import { KeysNotOfType } from "./KeysNotOfType";

type PickMembersNotOfType<Terface, Filter> = PickMembers<Terface, KeysNotOfType<Terface, Filter>>;

export { PickMembersNotOfType };

type PickNonVoidMembers<Terface> = PickMembersNotOfType<Terface, void>;

export { PickNonVoidMembers };

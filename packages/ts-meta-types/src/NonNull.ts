/**
 * Removes `null` from a type, keeping all other values.
 *
 * @template T - The type to remove `null` from
 * @returns A type excluding `null` while preserving all other values of `T`
 *
 * @example
 * ```typescript
 * type Example = NonNull<string | number | null>; // string | number
 * type Example2 = NonNull<null>; // never
 * ```
 */
export type NonNull<T> = T extends any ? (T extends null ? never : T) : never;

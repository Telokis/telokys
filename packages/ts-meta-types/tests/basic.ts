import { ValueOf, PickNonVoidMembers } from "../src/index";

interface Base {
    arg: number;
    arg2: string;
    arg3: void;
}

type Test = ValueOf<Base>;
type TTT = PickNonVoidMembers<Base>;

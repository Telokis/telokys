import { NonNull, MakeNonNull, Debug } from "../src/index";

interface Base {
    arg: number;
    arg2: string | null;
    arg2_2: string | null;
    arg3: null | {
        arg4: number | null;
        arg5: string;
    };
}

type Test1 = NonNull<string | number | null>;
type Test2 = MakeNonNull<Base, "arg2" | "arg3">;

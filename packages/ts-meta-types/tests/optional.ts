import { MakeOptional, Debug } from "../src/index";

interface Base {
    arg: number;
    arg2: string | null;
    arg2_2?: string | null;
    arg3: {
        arg4: number | null;
        arg5: string;
    };
}

type Test1 = Debug<MakeOptional<Base, "arg2" | "arg3">>;

module.exports = {
    env: {
        node: false,
        browser: true,
    },
    extends: ["airbnb", "airbnb/hooks", "@telokys/eslint-config"],
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        sourceType: "module",
    },
    ignorePatterns: ["node_modules"],
    settings: {
        "import/extensions": [".js", ".jsx"],
    },
    rules: {
        "import/extensions": [
            "error",
            "always",
            {
                js: "never",
                jsx: "never",
            },
        ],
        "jsx-a11y/aria-role": [
            "error",
            {
                ignoreNonDOM: true,
            },
        ],
        "jsx-a11y/click-events-have-key-events": "off",
        "jsx-a11y/label-has-associated-control": "off",
        "jsx-a11y/label-has-for": "off",
        "jsx-a11y/no-static-element-interactions": "off",
        "react/destructuring-assignment": "off",
        "react/no-unused-state": "off", // False positives
        "react/display-name": "off",
        "react/forbid-foreign-prop-types": "off",
        "react/forbid-prop-types": ["error", { forbid: ["any"] }],
        "react/jsx-filename-extension": "off",
        "react/no-access-state-in-setstate": "off",
        "react/no-did-mount-set-state": "off",
        "react/sort-comp": "off",
        "react/jsx-props-no-spreading": "off",
    },
};

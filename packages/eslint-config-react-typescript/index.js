module.exports = {
    extends: ["@telokys/eslint-config-react", "@telokys/eslint-config-typescript"],
    settings: {
        "import/resolver": {
            node: {
                extensions: [".js", ".ts", ".jsx", ".tsx"],
            },
        },
        "import/extensions": [".js", ".ts", ".jsx", ".tsx"],
    },
    rules: {
        "import/extensions": [
            "error",
            "always",
            {
                js: "never",
                ts: "never",
                jsx: "never",
                tsx: "never",
            },
        ],
        "react/require-default-props": "off",
    },
    overrides: [
        {
            files: [
                "**/tests/**/*.test.js",
                "**/__tests__/**/*.test.js",
                "**/tests/**/*.test.ts",
                "**/__tests__/**/*.test.ts",
                "**/tests/**/*.test.jsx",
                "**/__tests__/**/*.test.jsx",
                "**/tests/**/*.test.tsx",
                "**/__tests__/**/*.test.tsx",
            ],
            extends: [require.resolve("./jest")],
        },
        {
            files: ["webpack.config.js", ".babelrc.js", ".eslintrc.js"],
            env: {
                node: true,
                browser: false,
            },
        },
    ],
};

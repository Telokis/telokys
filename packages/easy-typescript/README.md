# `easy-typescript`

**This project is heavily opinionated**

Simply initialize a project to enable TypeScript.  
Run `npx @telokys/easy-typescript` and answer the questions to guide the setup.

## Process

This script will do the following steps:

-   Update your `package.json` to add custom scripts and update the `main` and `types` properties. (See [the changes](https://gitlab.com/Telokis/telokys/-/blob/master/packages/easy-typescript/src/index.ts#L25))
-   Install the dev dependencies required to enable TypeScript (See [the list](https://gitlab.com/Telokis/telokys/-/blob/master/packages/easy-typescript/src/index.ts#L11))
-   Create a `tsconfig.json` file. (See [the content](https://gitlab.com/Telokis/telokys/-/blob/master/packages/easy-typescript/src/index.ts#L51))
-   Create a `.babelrc.js` file. (See [the content](https://gitlab.com/Telokis/telokys/-/blob/master/packages/easy-typescript/src/index.ts#L76))

## Options

### yes

Passing `--yes` to the program will skip the prompts and act as if you had answered `yes` to them all.

## Tests

If you want to setup tests for your project, you simply need to `npm install -D jest`.  
Your test files should be inside a `tests` directory (possibly nested) with the suffix `.test.js` or `.test.ts`.  
Once you're done, you can use `npm run test`, `npm run test:coverage` or `npm run test:silent` to run your tests.

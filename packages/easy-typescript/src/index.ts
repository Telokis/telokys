import fs from "fs";
import path from "path";
import chalk from "chalk";
import { execSync } from "child_process";
import minimist from "minimist";
import { PackageJson, updateProperty } from "./packageJson";
import { confirm, iterateProperties, Iterator, createFile } from "./helpers";

const argv = minimist(process.argv.slice(2));

const deps = [
    "cross-env",
    "@types/jest",
    "babel-jest",
    "@babel/cli",
    "@babel/core",
    "@babel/node",
    "@types/node",
    "ts-node",
    "typescript",
    "@telokys/babel-preset-typescript",
]
    .map(d => `"${d}"`)
    .join(" ");

const packageJsonNewProperties = {
    scripts: {
        "check-types": "tsc --noEmit && echo Type checking ok",
        lint: "eslint --ext .js,.ts . && echo Linting ok",
        "lint:fix": "eslint --fix --ext .js,.ts . && echo Linting ok",
        start: "babel-node src/index.ts -x .ts,.js --ignore __fake__",
        build: "npx cross-env NODE_ENV=production babel . --only src -d build -x .ts,.js",
        "build:declaration":
            "tsc --emitDeclarationOnly --outDir build && echo Emitted declaration files",
        prepublishOnly: "npm run build && npm run build:declaration",
        test: "jest",
        "test:silent": "jest --silent",
        "test:coverage": "jest --coverage",
    },
    main: "build/src/index.js",
    types: "build/src/index.d.ts",
    jest: {
        testMatch: [
            "**/tests/**/*.test.js",
            "**/__tests__/**/*.test.js",
            "**/tests/**/*.test.ts",
            "**/__tests__/**/*.test.ts",
        ],
    },
};

const defaultTsConfig = `{
    "compilerOptions": {
        "allowJs": false,
        "allowSyntheticDefaultImports": true,
        "experimentalDecorators": true,
        "declaration": true,
        "lib": ["es2017"],
        "module": "commonjs",
        "moduleResolution": "node",
        "noImplicitAny": false,
        "noUnusedLocals": true,
        "noUnusedParameters": false,
        "preserveConstEnums": true,
        "removeComments": false,
        "resolveJsonModule": true,
        "skipLibCheck": true,
        "sourceMap": true,
        "strict": true,
        "strictFunctionTypes": false,
        "target": "esnext",
        "rootDir": "."
    },
    "exclude": ["**/node_modules/*", "build", "coverage", "dist"]
}`;

const defaultBabelRc = `module.exports = {
    presets: ["@telokys/babel-preset-typescript"],
};`;

const files = [
    [".babelrc.js", defaultBabelRc],
    ["tsconfig.json", defaultTsConfig],
];

const main = async () => {
    const packageJsonPath = path.resolve(process.cwd(), "package.json");

    if (!fs.existsSync(packageJsonPath)) {
        throw new Error(
            "There is no package.json in your current directory. Did you forget to run npm init?",
        );
    }

    console.log(`Loading the ${chalk.cyan("package.json")}`);
    const packageJson = JSON.parse(fs.readFileSync(packageJsonPath, "utf8")) as PackageJson;
    const propertyIteratorData: Parameters<Iterator>[] = [];

    iterateProperties(packageJsonNewProperties, (newValue, valuePath, get, set) => {
        propertyIteratorData.push([newValue, valuePath, get, set]);
    });

    for (let i = 0; i < propertyIteratorData.length; i++) {
        const [newValue, valuePath, get, set] = propertyIteratorData[i];

        await updateProperty(
            val => {
                set(packageJson as {}, val);
                console.log(`Property ${chalk.yellow(valuePath)} marked for update.`);
            },
            valuePath,
            get(packageJson as {}),
            newValue,
            argv.yes === true,
        );

        console.log();
    }

    console.log(`Updating your ${chalk.cyan("package.json")}`);
    fs.writeFileSync(packageJsonPath, JSON.stringify(packageJson, null, 4), "utf8");
    console.log(`${chalk.cyan("Package.json")} updated.\n`);

    if (argv.yes === true || (await confirm("Do you want to install the dev dependencies?"))) {
        console.log(`Installing ${chalk.cyan("dev dependencies")}.`);
        console.log(`> npm install -D ${chalk.cyan(deps)}`);
        execSync(`npm install -D ${deps}`);
        console.log(`${chalk.cyan("Dev dependencies")} installed.\n`);
    } else {
        console.log(`Skipping ${chalk.cyan("dev dependencies")}.`);
    }

    for (let i = 0; i < files.length; i++) {
        const [file, content] = files[i];
        const filePath = path.resolve(process.cwd(), file);

        await createFile(file, filePath, content, argv.yes === true);
    }
};

export default main;

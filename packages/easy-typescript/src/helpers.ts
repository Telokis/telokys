import prompts from "prompts";
import fs from "fs";
import chalk from "chalk";

export async function confirm(message: string): Promise<boolean> {
    return prompts({
        name: "value",
        type: "confirm",
        message,
        initial: true,
    }).then(({ value }) => value);
}

type Val = string | string[];

interface RecursiveObj {
    [key: string]: Val | RecursiveObj;
}

export type Iterator = (
    value: Val,
    keyPath: string,
    get: (obj: RecursiveObj) => Val | undefined,
    set: (obj: RecursiveObj, val: Val) => void,
) => void;

function findDeep(obj: RecursiveObj, path: string[]) {
    let cur: Val | RecursiveObj = obj;

    for (let i = 0; i < path.length; i++) {
        const key = path[i];

        if (!(key in (cur as {}))) {
            return undefined;
        }

        cur = cur[key];
    }

    return cur as string;
}

function setDeep(obj: RecursiveObj, val: Val, path: string[]) {
    let cur: Val | RecursiveObj = obj;

    for (let i = 0; i < path.length - 1; i++) {
        const key = path[i];

        if (!cur[key]) {
            cur[key] = {};
        }

        cur = cur[key];
    }

    cur[path[path.length - 1]] = val;
}

export function iterateProperties(obj: RecursiveObj, iterator: Iterator, path: string[] = []) {
    const entries = Object.entries(obj);

    for (let i = 0; i < entries.length; i++) {
        const [key, value] = entries[i];

        path.push(key);

        if (typeof value === "string" || Array.isArray(value)) {
            iterator(
                value,
                path.join("."),
                (p => o => findDeep(o, p))([...path]),
                (p => (o, val) => setDeep(o, val, p))([...path]),
            );
        } else {
            iterateProperties(value, iterator, path);
        }

        path.pop();
    }
}

export async function createFile(name: string, path: string, content: string, force: boolean) {
    console.log(`Creating the file "${chalk.yellow(name)}".`);
    if (!fs.existsSync(path) || force) {
        fs.writeFileSync(path, content, "utf8");
    } else {
        console.log(`The file "${chalk.yellow(name)}" already exists.`);
        if (await confirm("Do you want to overwrite it?")) {
            fs.writeFileSync(path, content, "utf8");
            console.log(`The file "${chalk.yellow(name)}" has been overwritten.`);
        } else {
            console.log(`The file "${chalk.yellow(name)}" was skipped.`);
        }
    }
}

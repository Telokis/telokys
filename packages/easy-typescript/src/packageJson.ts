import { confirm } from "./helpers";
import chalk from "chalk";

export interface PackageJson {
    scripts?: {
        [key: string]: string;
    };
    main?: string;
    types?: string;
}

export async function updateProperty(
    setter: (value: string | string[]) => void,
    name: string,
    currentVal: string | string[] | null | undefined,
    newVal: string | string[],
    force: boolean,
) {
    console.log(`Checking the ${chalk.yellow(name)} property.`);
    if (!currentVal || force) {
        setter(newVal);
    } else if (currentVal !== newVal) {
        console.log(
            `A value is already defined for ${chalk.yellow(name)} in your ${chalk.cyan(
                "package.json",
            )}.`,
        );
        console.log(
            "Do you want to do the following change? (If you refuse, this step will be skipped)",
        );
        console.log("From", chalk.red(JSON.stringify(currentVal)));
        console.log("To  ", chalk.green(JSON.stringify(newVal)));
        if (await confirm("Overwrite the value?")) {
            setter(newVal);
        } else {
            console.log("The value won't change.");
        }
    } else {
        console.log(`Property ${chalk.yellow(name)} already has the proper value.`);
    }
}

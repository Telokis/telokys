module.exports = () => {
    const env = process.env.BABEL_ENV || process.env.NODE_ENV || "development";

    const allPlugins = [
        require("@babel/plugin-transform-modules-commonjs"),
        [require("@babel/plugin-transform-for-of"), { loose: true }],
        [require("@babel/plugin-proposal-decorators"), { legacy: true }],
        require("@babel/plugin-proposal-numeric-separator"),
        require("@babel/plugin-proposal-optional-chaining"),
        require("@babel/plugin-proposal-nullish-coalescing-operator"),
        [
            require("babel-plugin-module-resolver"),
            {
                extensions: [".js", ".ts", ".tsx"],
            },
        ],
    ];

    const presetReact = require("@babel/preset-react");
    const pluginTransformTypeScript = require("@babel/plugin-transform-typescript");

    const res = {
        parserOpts: {
            strictMode: true,
        },
        plugins: allPlugins,
        overrides: [
            {
                test: /\.ts$/,
                plugins: [[pluginTransformTypeScript], ...allPlugins],
            },
            {
                test: /\.tsx$/,
                plugins: [[pluginTransformTypeScript, { isTSX: true }], ...allPlugins],
                presets: [presetReact],
            },
            {
                test: /\.jsx$/,
                presets: [presetReact],
            },
        ],
    };

    if (env === "debug") {
        res.retainLines = true;
        res.sourceMaps = "inline";
    }

    return res;
};

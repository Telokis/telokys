> Yeoman generator scaffolding a TypeScript project with dev tools and common files.

## Installation

You can simply use `npx` in order to run this generator without explicitely installing dependencies:

```bash
npx -p yo -p @telokys/generator-ts yo @telokys/ts
```

## License

MIT © [Telokis](https://gitlab.com/Telokis)

## Goals

-   Called with argument pointing to the directory of the project
    -   We resolve the directory using `process.cwd()`
    -   If the directory doesn't exist, we create it (recursively)
    -   If the directory exists, we throw an error if it isn't empty

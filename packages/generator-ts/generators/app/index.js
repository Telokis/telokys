const Generator = require("yeoman-generator");
const path = require("path");

class TSGenerator extends Generator {
    constructor(args, opts) {
        super(args, opts);

        this.argument("projectPath", {
            description: "Where the project should be initialized to. Default: current directory",
            type: String,
            required: false,
            default: ".",
        });

        this.projectPath = path.resolve(process.cwd(), this.options.projectPath);

        this.option("name", {
            description: "Name of the project to create",
            type: String,
            required: false,
            default: path.basename(this.projectPath),
        });

        this.log("this.options.projectPath:", this.projectPath);
        this.log("this.options.name:", this.options.name);
    }

    writing() {
        // this.fs.copy(this.templatePath("dummyfile.txt"), this.destinationPath("dummyfile.txt"));
    }

    install() {
        // this.installDependencies();
    }
}

exports.default = TSGenerator;
